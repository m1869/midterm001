/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.midterm001;

/**
 *
 * @author acer
 */
public class IDOL {
    private String Name;
    private String Position;
    private String Nationality;
    
    public IDOL (){
      System.out.println("IDOL");
    }
    public void setName(String name){
        this.Name = name;
        Check();
    }
    public String getName(){
        return Name;
    }
    public void R1SE() {
        System.out.println(">>>R1SE<<<");
        System.out.println("NUMBER1");
        System.out.println("Name : Zhou Zhennan");
        System.out.println("Position : Leader,Center,Rapper");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER2");
        System.out.println("Name : He Luoluo");
        System.out.println("Position : Vocals");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER3");
        System.out.println("Name : Yan Xujia");
        System.out.println("Position : Rapper");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER4");
        System.out.println("Name : Xia Zhiguang");
        System.out.println("Position : Dance");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER5");
        System.out.println("Name : Yao Chen");
        System.out.println("Position : Rapper,Dance");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER6");
        System.out.println("Name : Zhai Xiaowen");
        System.out.println("Position : Vocals");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER7");
        System.out.println("Name : Zhang Yanqi");
        System.out.println("Position : Rapper");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER8");
        System.out.println("Name : Liu Ye");
        System.out.println("Position : Dance");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER9");
        System.out.println("Name : Ren Hao");
        System.out.println("Position : Rapper");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER10");
        System.out.println("Name : Zhao Lei");
        System.out.println("Position : Vocals");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER11");
        System.out.println("Name : Zhao Rang");
        System.out.println("Position : Dance");
        System.out.println("Nationality : Chinese");
    }
    public void BTS(){
        System.out.println(">>>BTS<<<");
        System.out.println("NUMBER1");
        System.out.println("Name : Kim Nam Joon");
        System.out.println("Position : Leader,Rapper");
        System.out.println("Nationality : Korean");
        System.out.println("NUMBER2");
        System.out.println("Name : Kim Seok Jin");
        System.out.println("Position : Vocal");
        System.out.println("Nationality : Korean");
        System.out.println("NUMBER3");
        System.out.println("Name : Min Yoon Gi");
        System.out.println("Position : Rapper");
        System.out.println("Nationality : Korean");
        System.out.println("NUMBER4");
        System.out.println("Name : Jung Ho Seok");
        System.out.println("Position : Rapper,Dance");
        System.out.println("Nationality : Korean");
        System.out.println("NUMBER5");
        System.out.println("Name : Park Ji Min");
        System.out.println("Position : Vocal,Dance");
        System.out.println("Nationality : Korean");
        System.out.println("NUMBER6");
        System.out.println("Name : Kim Tae Hyung");
        System.out.println("Position : Vocal,Dance");
        System.out.println("Nationality : Korean");
        System.out.println("NUMBER7");
        System.out.println("Name : Jeon Jung Kook");
        System.out.println("Position : Vocal,Dance,Center");
        System.out.println("Nationality : Korean");
    }
    public void All(){
        System.out.println(">>>R1SE<<<");
        System.out.println("NUMBER1");
        System.out.println("Name : Zhou Zhennan");
        System.out.println("Position : Leader,Center,Rapper");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER2");
        System.out.println("Name : He Luoluo");
        System.out.println("Position : Vocals");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER3");
        System.out.println("Name : Yan Xujia");
        System.out.println("Position : Rapper");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER4");
        System.out.println("Name : Xia Zhiguang");
        System.out.println("Position : Dance");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER5");
        System.out.println("Name : Yao Chen");
        System.out.println("Position : Rapper,Dance");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER6");
        System.out.println("Name : Zhai Xiaowen");
        System.out.println("Position : Vocals");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER7");
        System.out.println("Name : Zhang Yanqi");
        System.out.println("Position : Rapper");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER8");
        System.out.println("Name : Liu Ye");
        System.out.println("Position : Dance");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER9");
        System.out.println("Name : Ren Hao");
        System.out.println("Position : Rapper");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER10");
        System.out.println("Name : Zhao Lei");
        System.out.println("Position : Vocals");
        System.out.println("Nationality : Chinese");
        System.out.println("NUMBER11");
        System.out.println("Name : Zhao Rang");
        System.out.println("Position : Dance");
        System.out.println("Nationality : Chinese");
        System.out.println(">>>BTS<<<");
        System.out.println("NUMBER1");
        System.out.println("Name : Kim Nam Joon");
        System.out.println("Position : Leader,Rapper");
        System.out.println("Nationality : Korean");
        System.out.println("NUMBER2");
        System.out.println("Name : Kim Seok Jin");
        System.out.println("Position : Vocal");
        System.out.println("Nationality : Korean");
        System.out.println("NUMBER3");
        System.out.println("Name : Min Yoon Gi");
        System.out.println("Position : Rapper");
        System.out.println("Nationality : Korean");
        System.out.println("NUMBER4");
        System.out.println("Name : Jung Ho Seok");
        System.out.println("Position : Rapper,Dance");
        System.out.println("Nationality : Korean");
        System.out.println("NUMBER5");
        System.out.println("Name : Park Ji Min");
        System.out.println("Position : Vocal,Dance");
        System.out.println("Nationality : Korean");
        System.out.println("NUMBER6");
        System.out.println("Name : Kim Tae Hyung");
        System.out.println("Position : Vocal,Dance");
        System.out.println("Nationality : Korean");
        System.out.println("NUMBER7");
        System.out.println("Name : Jeon Jung Kook");
        System.out.println("Position : Vocal,Dance,Center");
        System.out.println("Nationality : Korean");
    }
    public void Check(){
        if(Name.equals("R1SE")){
            R1SE();
        }else if (Name.equals("BTS")){
            BTS();
        }else{
            All();
        }
    } 
}
